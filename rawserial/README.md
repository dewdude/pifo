Please see the main README.md for full project details, ignoring references to hamlib. 

This is the raw serial driver version of PiFO. Compared to the hamlib version, it only works for iCom CI-V but is capable of
much faster channel/vfo flipping. If you are using an iCom radio, you are more than welcome to give this version a try. If
you have a different radio, you may be able to convert it to work by swapping in the proper bytes. 

Most of the methods and commands have been at least tested on a IC-7300. Most of the development work of this version is done
on an IC-725 running in IC-735/731 compatibility mode. This means the system uses only 4-bytes for frequency data as opposed
to five. Should your radio not support 4-bytes for the VFO, the code can be easily adapted. I fully suspect given the fact a
frame ends in 0xFD that your radio can probably understand it. This would only matter when I start pulling data from the radio.

I have essentially "seperated" the two as I felt accessibility might be more important, at least at first, than speed. Yes, this
is faster, but it's also been written for CI-V. If you've got an Elcraft, or Kenwood, or Yaesu; then not only does it do you
no good, but you might be less inclined to want to help develop. A lot more people will be able to make use of this with less
work if I make the Hamlib build the main one. Others might be more inclined to get involved if "it benefits them".

I will, for the forseeable future; continue to develop at least a CI-V version for my own personal use. It will likely lag behind
the hamlib as far as new features. That's largely because development using Hamlib is 1000% easier. It might also be better to
focus on implementing a bunch of stuff to have the look/feel before I start mixing in how I'm going to handle a bunch of raw
serial data. 


CHANGELOG:

29-May-2019 - Split from main version. Moved CHANGELOG.