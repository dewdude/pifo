#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# PiFO de NQ4T/Jay Moore
# RawSerial Driver v.3 (iCom CI-V Version)

import RPi.GPIO as GPIO, time
import tm1637
import serial

GPIO.setwarnings(False)

Display = tm1637.TM1637(CLK=4, DIO=5, brightness=1.0) # Change to match your GPIO pins.
Display.Clear()

Enc_A = 17   			# GPIO Pin attached to one switch on encoder
Enc_B = 18  			# GPIO Pin attached to other switch on encoder.
lastitem = (0,1,1)		# Rotary encoder code...I wouldn't touch it.

chan = 19	# Default channel & frequency list
ctable = [26965000, 26965000, 26975000, 26985000, 27005000, 
27015000, 27025000, 27035000, 27055000, 27065000, 27075000, 
27085000, 27105000, 27115000, 27125000, 27135000, 27155000, 
27165000, 27175000, 27185000, 27205000, 27215000, 27225000, 
27255000, 27235000, 27245000, 27265000, 27275000, 27285000, 
27295000, 27305000, 27315000, 27325000, 27335000, 27345000, 
27355000, 27365000, 27375000, 27385000, 27395000, 27405000]


# pySerial configuration
# Use ttyAMA0 for hardware UART, /dev/ttyS0 for miniuart
# (or the alias might work too.)
ser = serial.Serial(
        port='/dev/ttyAMA0',
        baudrate = 9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout = 1
)


def channelup():
	global chan
	if chan==40:	# Rolls over so up from 40 is 1.
		chan = 1
	else:
		  chan += 1
	setradio()
	display()

def channeldown():
	global chan
	if chan==1:	# Like the above, but for going down.
		chan = 40
	else:
		chan -= 1
	setradio()
	display()

def display():
	global chan
	disp = [int(x) for x in str(chan).zfill(2)] 	# We need a list, and we need the leading 0.
	Display.Show1(1, disp[0]) 	# Update digit
	Display.Show1(2, disp[1])	# Update digit

def setradio():
	global chan
	global ctable
	preamble = b'\xFE\xFE\x28\xE0\x00' # Contains everything except frequency data.
	eol = b'\xFD' 			# Marks the end of transmission.
	freq = str(ctable[chan]) 	# Crazy junk to create the BCD data I need
	f2 = [freq[i:i+2] for i in range(0, len(freq), 2)][::-1]
	freq = ''.join(str(f).zfill(2) for f in f2).decode("hex")
	cmd = preamble + freq + eol 	# Parse the bytes.
	ser.write(cmd) 			# Blast it to the radio.


# I found this somewhere while looking for rotary encoder code. Minor modifications to change what it does.
def init():
	GPIO.setwarnings(True)
	GPIO.setmode(GPIO.BCM)			# Use BCM mode
	GPIO.setup(Enc_A, GPIO.IN) 			# setup callback thread for the A and B encoder 	
	GPIO.setup(Enc_B, GPIO.IN)			
	GPIO.add_event_detect(Enc_A, GPIO.RISING, callback=rotary_interrupt) 	# NO bouncetime 
	GPIO.add_event_detect(Enc_B, GPIO.RISING, callback=rotary_interrupt) 	# NO bouncetime 

# 'interrupt' handler
def rotary_interrupt(w):			
	global lastitem
	
	if w == Enc_A:
		item = (w, 1, GPIO.input(Enc_B))
	else:
		item = (w, GPIO.input(Enc_A),1)
		
	if item == (Enc_A,1,1) and lastitem[1] == 0:	# Is it in END position?
		channeldown()
	elif item == (Enc_B,1,1) and lastitem[2] == 0:	# Same but for ENC_B
		channelup()
	lastitem = item	

# init and loop forever (stop with CTRL C)
setradio()		# initialize radio and display to default
display()
init()
while 1:
	time.sleep(1)
