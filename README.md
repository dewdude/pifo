PiFO de NQ4T - nq4tango (at) gmail

# PiFO Discontination

The "replacement" version is called ArFO; it can be found at 
https://gitlab.com/dewdude/arfo

Though I said this a few months ago and changed my mind; I have decided to actually discontinue PiFO for good. 

When I first came up with the idea for PiFO, it largely grew out of something to just make monitoring CB channels on my
725 a bit easier; this is why the "proof of concept" was channelized for CB. I honestly wasn't even sure if I would be
able to actually accomplish such a task as I'm not much of a programmer. 

At the time going with a RaspberryPi seemed like it would be the easiest way of doing it; utilize the underlying OS and
existing library of tools to cobble something together. The initial version did this by calling HamLib's rigctl program
from within Python. But I was never happy with the results; even after getting my 725 working at 9600 baud, it was too
slow. Hamlib did things "by the book"; waited for echo, waited for rig response, sent the "long form" every time. I cared 
more about making things work untill they break. 

The "rawserial" version spawned from this; ignoring echos or replies we just continiously blast data to the radio. It works,
and it worked better than I imagined. Even better was the fact that no matter how many times I spun that knob, or how fast I 
spun it; the radio seemed to respond. In fact, most of my test scripts were just sending VFO changes one after another with 
no delay between them. Both my 725 and 7300 did fine with this. To break the rules even more; I was sending the old 4-bytes
of BCD data for the frequency rather than the full 5. In most cases, this is called "731 Compatibility Mode" as the IC-731/735, 
the first radios that supported CI-V, only use 4 bytes. The 725 before I changed it's settings and my 7300 gladly accepted 4
bytes of frequency data. Since I wasn't running a mixed enviroment of radios...I figured this wasn't a big deal.

However I've started getting in to Arduino; and while I'm far from an expert in it's programming language...I've progressed far
enough that I can do something like PiFO with just a little extra work; and I can probably make it function the way I thought 
about learning how to do in Python.

I recently ordered an Arduino Mega and some misc parts to start building a hardware interface between Arduino and CI-V (though I
suspect the one for RPi will work too), and will start assembling some code. I will update this when I get a repository created.

# About

PiFO (Pi Frequency Oscillator) is a project for controlling a ham radio transceiver over serial (or USB I guess) for the purposes
of controling the radio with a "remote head". It's current early stage acts as a "memory unit"; allowing you to spin a knob to
scroll through "channels" while sending the proper commands to the radio. Further features, functions, and refinement will occur.

The current release consists of a HamLib based version as well as a pySerial version that speaks CI-V directly. I've determined the
pySerial method will pose more of a challenge at the advantage of more efficent/snappier operation. The HamLib version will be 
available for rigs that don't have a pySerial version developed yet; as well as to speed up development of features and the external
interface. The hardware interface currently uses a TM1637 based 4-digit LED display. 

It's written in Python. Hamlib version requires Hamlib and it's Python bindings. pySerial version requires just pySerial. It's
development is focused largely on working with the iCom IC-725; though some aspects have been tested with an IC-7300.

# Requirements

Hamlib: You will need a Raspberry Pi, a rig compatible with Hamlib, some sort of interface so the rig can talk to the Pi, and to modify the
code a little to setup your radio. 

Rawserial: An Rpi, an iCom CI-V compatible radio, the interface to go from the Pi to the rig, and to configure the serial port in the script.

The iCom rawserial version has been tested with both hardware UART serial on a RPi3B and a Pi Zero W. The same version has also been
run using the miniuart serial on a Pi Zero W with moderate success; it works well but prone to crashing. I currently use it on a Pi0W
with hardware UART using a read-only setup. Instead of cron, it's using systemd to start it up. It's up and functional within 20 secs
of plugging it up to power. 

The Hamlib version has only been tested using hardware UART. I can't comment on how it works on miniuart. 

![Raspberry Pi to CI-V interface](/images/civ-interface.png)

This is the simple circuit used to go from the RaspberryPi's serial interface to the open-collector one-wire CI-V bus.
The fastest I could run my interface (built on prototype board) was 9600 baud on a IC-7300, 19200 failed or isn't supported over the
remote port. I had thought my 725 was limited to 1200; but I found it'd do 9600 if I modified some diodes. Upping the baud rate and
programming the pySerial version eliminated workarounds I had due to the hamlib and 1200 baud lag.

# Operation

The program is set up to read the rotary encoder pins as an interrupt and determine the direction of rotation (up or down). It
then calls either the channelup or channeldown functions; which check to make sure the channel number shouldn't be rolling over
and increments it otherwise. What happens next depends on the version:

Hamlib: I basically tell hamlib to set the VFO according to the channel index in the 'ctable' list. After it's finished, we set
the display.

rawserial: Read the ctable index for the channel, do some crazy stuff to generate the proper BCD data, convert it to hex, merge
the bytes of the preamble/address/command with the frequency bytes and the end-of-command byte. Then we set the display.

The Hamlib version is "wasteful" in my opinion. It sends not only a change VFO command (I think), but waits for the echo and ack
before sending the frequency...waiting for the echo and ack before exiting. This is quite a few wasted bytes, and the effect is
somewhat noticable if you start spinning the rotary encoder quickly.

The rawserial version, OTOH, doesn't even have provisions for listening (yet). It will just send the VFO change command (one that the
radio doesn't return an ack for) over and over and over. The result is we are unable to spin the knob faster than we can send commands;
and the radio doesn't seem to care about sending it's echo if it's getting a giant string of commands.In it's current state the PiFO 
mimics the channel operation of a CB radio. It can be customized for as many channels as one wants by changing the roll-over number in
channelup/down and creating a table of frequencies. 

In the future, PiFO will move beyond this "proof of concept". It will be able to work with the VFO directly and allow you to customize
the frequency step-rate, regardless of what the radio itself supports. The ability to read and change operating modes will also be added.

# Pictures and Media

![Breadboard of CI-V Interface](/images/civ-breadboard.jpg)*Breadboard Prototype of CI-V Interface*
![Detail of CI-V Breadboard](/images/civ-breaboarddetail.jpg)*Breadboard Detail*
![First ProtoHat Version](/images/civ-protoboard-initial.jpg)*Version Built on ProtoHat*
![First ProtoHat Rear](/images/civ-protoboard-initial-back.jpg)*Backside of ProtoHat*
![Remote Display and Encoder](/images/civ-encoderdisplay.jpg)*Display and Encoder Board*
![Remote Display and Encoder Rear](/images/civ-encoderdisplay-back.jpg)*Display and Encoder Board Back*
![Updated ProtoHat](/images/civ-protohat-updated.jpg)*Updated ProtoHat for Remote Board*
![Updated ProtoHat Rear](/images/civ-protohat-updated-back.jpg)*Updated ProtoHat Back*

A video of the improved version at 9600 baud running on the IC-725 can be found here:
https://www.youtube.com/watch?v=5DIgHiW0fjY

# History

```
17-May-2019: Initial prototyping of CI-V interface and rigctl test.
19-May-2019: CI-V board constructed, initial LED tests.
20-May-2019: rotary encoder workings, initital rigctl based version, constructed LED/encoder board
25-May-2019: intital raw serial version
26-May-2019: Improved pySerial version
27-May-2019: Change to 9600 baud. Elimination of 100ms update rate. Much fast. Very Wow.
29-May-2019: "The Great Split" - "Officially" using Hamlib. Moved rawserial to it's own folder.
06-June-2019: Ignore some warnings to fix start-at-boot-problems. Tested with mini-UART.
30-June-2019: Discontinuation of Project - Will no longer be developed.
17-July-2019: Discontinuation of discontinuation. Project will continue....slowly
````
 
TM1637 module code covered by MIT License. See tm1637.license for full details
Thanks to Vance N3VEM for inspiration for the pyserial version

# BSD 2-clause "Simplified" License

Copyright 2019 Jay Moore

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following 
conditions are met: 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the 
following disclaimer. 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
the following disclaimer in the documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, 
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
